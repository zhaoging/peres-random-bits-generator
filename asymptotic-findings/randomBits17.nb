(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     22854,        567]
NotebookOptionsPosition[     17901,        477]
NotebookOutlinePosition[     18333,        494]
CellTagsIndexPosition[     18290,        491]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"G0", "=", "0"}], ";"}]], "Input",
 CellChangeTimes->{{3.76519269943666*^9, 3.765192702776413*^9}},
 CellLabel->"In[1]:=",ExpressionUUID->"622637be-9c56-4453-9453-78e2f9f782d5"],

Cell[BoxData[
 RowBox[{
  RowBox[{"G1", "=", "1"}], ";"}]], "Input",
 CellChangeTimes->{{3.7619947681296234`*^9, 3.7619948058677197`*^9}, {
  3.7619948361743064`*^9, 3.7619948407916293`*^9}},
 CellLabel->"In[2]:=",ExpressionUUID->"4b678650-a286-4fdf-80d5-ce76be160192"],

Cell[BoxData[
 RowBox[{
  RowBox[{"G2", "=", 
   RowBox[{"3", "/", "2"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.798864024507595*^9, 3.798864031707519*^9}, 
   3.7988699099902563`*^9, 3.7988853403635073`*^9},
 CellLabel->"In[3]:=",ExpressionUUID->"4ba2c2cc-6779-4d14-a0d8-237476e35efd"],

Cell[BoxData[
 RowBox[{
  RowBox[{"G3", "=", 
   RowBox[{"5", "/", "2"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.798864418589789*^9, 3.798864422436969*^9}, 
   3.798869912620517*^9, 3.798885343339119*^9},
 CellLabel->"In[4]:=",ExpressionUUID->"71da341b-2543-4c5f-bd38-1907c4a5a79f"],

Cell[BoxData[
 RowBox[{
  RowBox[{"G", "[", "n_", "]"}], ":=", 
  RowBox[{"Block", "[", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"n0", "=", "n"}], "}"}], ",", "\[IndentingNewLine]", 
    RowBox[{"If", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"Mod", "[", 
        RowBox[{"n", ",", "2"}], "]"}], "\[Equal]", "0"}], ",", 
      RowBox[{
       RowBox[{"ToExpression", "[", 
        RowBox[{"\"\<G\>\"", "<>", 
         RowBox[{"ToString", "[", 
          RowBox[{"n", "/", "2"}], "]"}]}], "]"}], "+", 
       RowBox[{"Sum", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"Binomial", "[", 
           RowBox[{
            RowBox[{"n", "/", "2"}], ",", "k"}], "]"}], 
          RowBox[{
           RowBox[{"(", 
            RowBox[{"ToExpression", "[", 
             RowBox[{"\"\<G\>\"", "<>", 
              RowBox[{"ToString", "[", "k", "]"}]}], "]"}], ")"}], "/", 
           RowBox[{"2", "^", 
            RowBox[{"(", 
             RowBox[{"n", "/", "2"}], ")"}]}]}]}], ",", 
         RowBox[{"{", 
          RowBox[{"k", ",", "1", ",", 
           RowBox[{"n", "/", "2"}]}], "}"}]}], "]"}], "+", "1"}], ",", 
      RowBox[{"ToExpression", "[", 
       RowBox[{"\"\<G\>\"", "<>", 
        RowBox[{"ToString", "[", 
         RowBox[{"n", "-", "1"}], "]"}]}], "]"}]}], "]"}]}], 
   "\[IndentingNewLine]", "]"}]}]], "Input",
 CellChangeTimes->{{3.761994849454873*^9, 3.7619948718035917`*^9}, 
   3.7619949091777563`*^9, {3.7619949482487946`*^9, 3.761994950472999*^9}, {
   3.761995764579337*^9, 3.7619957969832077`*^9}, {3.761996131581234*^9, 
   3.761996189043539*^9}, {3.7651927302786183`*^9, 3.765192730700712*^9}, {
   3.765193968073866*^9, 3.765193968423945*^9}, {3.798863909678583*^9, 
   3.798863910077229*^9}},
 CellLabel->"In[5]:=",ExpressionUUID->"9cf03266-3f89-40ea-8d15-569937221b3d"],

Cell[BoxData[
 RowBox[{"Monitor", "[", 
  RowBox[{
   RowBox[{"Do", "[", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"ToExpression", "[", 
      RowBox[{"\"\<G\>\"", "<>", 
       RowBox[{"ToString", "[", "n", "]"}], "<>", "\"\<=G[\>\"", "<>", 
       RowBox[{"ToString", "[", "n", "]"}], "<>", "\"\<];\>\""}], "]"}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{"(*", 
      RowBox[{
       RowBox[{"AppendTo", "[", 
        RowBox[{"list2", ",", 
         RowBox[{"ToExpression", "[", 
          RowBox[{"\"\<f\>\"", "<>", 
           RowBox[{"ToString", "[", "n", "]"}]}], "]"}]}], "]"}], ","}], 
      "*)"}], "\[IndentingNewLine]", 
     RowBox[{"{", 
      RowBox[{"n", ",", "4", ",", "16384"}], "}"}]}], "\[IndentingNewLine]", 
    "]"}], ",", "n"}], "]"}]], "Input",
 CellChangeTimes->{{3.757076007368168*^9, 3.757076010395358*^9}, 
   3.7570802750238333`*^9, {3.757080638202567*^9, 3.757080643521158*^9}, {
   3.7571140156864786`*^9, 3.7571140322412386`*^9}, {3.757115245938142*^9, 
   3.757115272404065*^9}, {3.757118116037061*^9, 3.757118126941003*^9}, {
   3.7571254585739264`*^9, 3.7571254822129745`*^9}, {3.761996224359934*^9, 
   3.7619962421623473`*^9}, {3.761996729477708*^9, 3.7619967300522676`*^9}, {
   3.7619968032727456`*^9, 3.7619968033980722`*^9}, {3.7619972530855837`*^9, 
   3.761997253873934*^9}, {3.7620410803613844`*^9, 3.762041082695779*^9}, {
   3.764868202827445*^9, 3.764868203039485*^9}, {3.765066601767643*^9, 
   3.7650666019503675`*^9}, {3.765066731763732*^9, 3.76506673250636*^9}, {
   3.765068830182396*^9, 3.765068838063049*^9}, {3.7651927494909515`*^9, 
   3.765192749695998*^9}, 3.765194321880158*^9, 3.79863930440378*^9, {
   3.7986408210575533`*^9, 3.798640843650113*^9}, {3.798640995550284*^9, 
   3.798640998622458*^9}, 3.7986411102611103`*^9, {3.798863919736308*^9, 
   3.79886392147814*^9}, {3.798864428447885*^9, 3.7988644285900097`*^9}, 
   3.798869101679311*^9, 3.798869138763719*^9, {3.798885352613008*^9, 
   3.798885357204427*^9}},
 CellLabel->"In[9]:=",ExpressionUUID->"b0136dc1-2f57-485e-9e7b-44db92e222a2"],

Cell[BoxData["G7129"], "Input",
 CellChangeTimes->{{3.765194847039522*^9, 3.765194868797823*^9}},
 CellLabel->"In[37]:=",ExpressionUUID->"e46e6da7-213a-4a2e-a523-af44086e25db"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"G10537", "===", "G10536"}]], "Input",
 CellChangeTimes->{{3.7986680629806767`*^9, 3.798668071683442*^9}, {
  3.798678169980751*^9, 3.798678184292602*^9}},
 CellLabel->"In[18]:=",ExpressionUUID->"e64e6eb3-4371-4db2-b78b-9ca3176602f4"],

Cell[BoxData["True"], "Output",
 CellChangeTimes->{{3.7986680671146593`*^9, 3.798668072047494*^9}, 
   3.798675348039784*^9, {3.7986781651638308`*^9, 3.7986781848737593`*^9}},
 CellLabel->"Out[18]=",ExpressionUUID->"32a5300b-202b-451b-833e-5166ffc1c7b0"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Directory", "[", "]"}]], "Input",
 CellChangeTimes->{{3.798641123368237*^9, 3.798641125014398*^9}},
 CellLabel->"In[8]:=",ExpressionUUID->"e6231674-eef7-4b27-95f8-d7bec877664f"],

Cell[BoxData["\<\"/home/zhaoging/Dropbox/Research/RandomBits/Code\"\>"], \
"Output",
 CellChangeTimes->{{3.798641125965913*^9, 3.798641153981132*^9}, 
   3.7988912807584753`*^9},
 CellLabel->"Out[8]=",ExpressionUUID->"524c8248-3dac-465c-a466-8d3beb4158c3"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"SetDirectory", "[", "\"\</media/zhaoging/DATA\>\"", "]"}]], "Input",
 CellChangeTimes->{{3.7986411315604057`*^9, 3.7986411510738077`*^9}, {
  3.798950890273513*^9, 3.798950891044896*^9}},
 CellLabel->"In[1]:=",ExpressionUUID->"8c6272e2-e777-41cd-974f-2f66b810bdf9"],

Cell[BoxData["\<\"/media/zhaoging/DATA\"\>"], "Output",
 CellChangeTimes->{3.798641151668222*^9, 3.7986677848992767`*^9, 
  3.7986891604883137`*^9, 3.7988754934253073`*^9, 3.7988912843092318`*^9, 
  3.798950892110983*^9},
 CellLabel->"Out[1]=",ExpressionUUID->"6c11bb24-2cc9-43bf-bd45-f81d85c90633"]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"Save", "[", 
   RowBox[{"\"\<Gnew.mx\>\"", ",", "\"\<Global`\>\""}], "]"}], ";"}]], "Input",
 CellChangeTimes->{{3.798641155963616*^9, 3.7986412028324757`*^9}, 
   3.798675987941647*^9, {3.798891294475281*^9, 3.79889129556997*^9}},
 CellLabel->"In[10]:=",ExpressionUUID->"f3eb6af0-7685-48a7-8ae1-a0d1718ffff4"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Get", "[", "\"\<bigH.mx\>\"", "]"}], ";"}]], "Input",
 CellChangeTimes->{{3.798667787570462*^9, 3.79866781481669*^9}, 
   3.798678271488885*^9, {3.798689166299172*^9, 3.798689182645898*^9}, {
   3.798891415450839*^9, 3.7988914329171267`*^9}, 3.798891475066326*^9, {
   3.798950902192717*^9, 3.798950930537526*^9}},
 CellLabel->"",ExpressionUUID->"77190dd8-fec8-43e1-8a4d-7daef74f364e"],

Cell[CellGroupData[{

Cell[BoxData["G1000"], "Input",
 CellChangeTimes->{{3.765066737391095*^9, 3.765066742058906*^9}},
 CellLabel->
  "(Dialog) In[6]:=",ExpressionUUID->"7a845324-939a-4fb5-bd24-cc9479fc776b"],

Cell[BoxData[
 RowBox[{"59457192376801942321100943965482424161102285200220015909313622209241\
071359651345333495797543470539620014797419537575248270239171699217442181967279\
624451398983407882485270604647255005660665427073883217604046537284888541830681\
257852818107778383285947740221495330303286965570884050907171964944706275715", 
  "/", "6539969526283369878835602106079112613289824290194907271995546804018255\
927276221450764150261326268665329557329819049968415448884800368127707510118148\
619735598104594589126117544812667605628888636400118519380521530141346399699340\
06809031100094365055109531933378765047739725368031717079125173169291264"}]], \
"Output",
 CellChangeTimes->{
  3.765066742874817*^9, {3.7986393277557592`*^9, 3.7986393304273777`*^9}},
 CellLabel->
  "(Dialog) Out[6]=",ExpressionUUID->"acb0a4a1-2a4d-4121-bb79-29b863b7b9d3"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData["G1"], "Input",
 CellChangeTimes->{{3.7619962454760847`*^9, 3.7619962458982105`*^9}},
 CellLabel->"In[4]:=",ExpressionUUID->"b44b6267-35ef-4584-803f-c05853a1d8b2"],

Cell[BoxData["1"], "Output",
 CellChangeTimes->{3.761996246917369*^9},
 CellLabel->"Out[4]=",ExpressionUUID->"2441f3d2-2d9b-4e9e-89fb-bf9a2e86f134"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"G5", "+", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"5", "G1"}], "+", 
     RowBox[{"10", "G2"}], "+", 
     RowBox[{"10", "G3"}], "+", 
     RowBox[{"5", "G4"}], "+", "G5"}], ")"}], "/", "32"}]}]], "Input",
 CellChangeTimes->{{3.761996248093677*^9, 3.7619962547836046`*^9}, {
  3.761996903733683*^9, 3.7619969455718126`*^9}, {3.761996975947054*^9, 
  3.7619970424503107`*^9}},
 CellLabel->"In[16]:=",ExpressionUUID->"4624f07f-ec29-471a-b302-82cae5b9c0c7"],

Cell[BoxData[
 FractionBox["501", "128"]], "Output",
 CellChangeTimes->{3.7619962487679253`*^9, 3.761996946507902*^9, 
  3.7619970079798975`*^9, 3.761997043200613*^9},
 CellLabel->"Out[16]=",ExpressionUUID->"e63cf172-7c4f-4d75-a890-301418a2243f"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData["G11"], "Input",
 CellChangeTimes->{{3.76486821435675*^9, 3.764868217495334*^9}},
 CellLabel->"In[19]:=",ExpressionUUID->"8387b25b-ff0c-410b-873c-05faafb66bd6"],

Cell[BoxData[
 FractionBox["501", "128"]], "Output",
 CellChangeTimes->{3.7648682182809553`*^9, 3.798678204756022*^9},
 CellLabel->"Out[19]=",ExpressionUUID->"59d7dca0-8b27-4170-b794-d44642fcc2c7"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Array", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"ToExpression", "[", 
     RowBox[{"\"\<G\>\"", "<>", 
      RowBox[{"ToString", "[", 
       RowBox[{"#", "-", "1"}], "]"}]}], "]"}], "&"}], ",", "21"}], 
  "]"}]], "Input",
 CellChangeTimes->{{3.7619962577442284`*^9, 3.761996265557798*^9}, {
  3.7619967395277133`*^9, 3.7619967652888203`*^9}, {3.7619967970797663`*^9, 
  3.761996797245881*^9}, {3.7650666079773827`*^9, 3.765066608327199*^9}, {
  3.7651927588820696`*^9, 3.765192762256846*^9}},
 CellLabel->"In[23]:=",ExpressionUUID->"0a947f28-ffa5-46e5-b60e-d09ac427f944"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0", ",", "1", ",", 
   FractionBox["3", "2"], ",", 
   FractionBox["3", "2"], ",", 
   FractionBox["19", "8"], ",", 
   FractionBox["19", "8"], ",", 
   FractionBox["21", "8"], ",", 
   FractionBox["21", "8"], ",", 
   FractionBox["475", "128"], ",", 
   FractionBox["475", "128"], ",", 
   FractionBox["501", "128"], ",", 
   FractionBox["501", "128"], ",", 
   FractionBox["279", "64"], ",", 
   FractionBox["279", "64"], ",", 
   FractionBox["581", "128"], ",", 
   FractionBox["581", "128"], ",", 
   FractionBox["189627", "32768"], ",", 
   FractionBox["189627", "32768"], ",", 
   FractionBox["194535", "32768"], ",", 
   FractionBox["194535", "32768"], ",", 
   FractionBox["411969", "65536"]}], "}"}]], "Output",
 CellChangeTimes->{3.765192765002452*^9, 3.7986783044876547`*^9},
 CellLabel->"Out[23]=",ExpressionUUID->"9822dd6a-51b3-4cdb-b5af-391eb195cd42"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Array", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"ToExpression", "[", 
     RowBox[{"\"\<g\>\"", "<>", 
      RowBox[{"ToString", "[", 
       RowBox[{"#", "-", "1"}], "]"}]}], "]"}], "&"}], ",", "21"}], 
  "]"}]], "Input",
 CellChangeTimes->{3.798678283184757*^9},
 CellLabel->"In[7]:=",ExpressionUUID->"3461529b-98be-448f-a9e6-788a6185f5ee"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"0", ",", "1", ",", 
   FractionBox["3", "2"], ",", 
   FractionBox["5", "2"], ",", 
   FractionBox["19", "8"], ",", 
   FractionBox["27", "8"], ",", 
   FractionBox["15", "4"], ",", 
   FractionBox["19", "4"], ",", 
   FractionBox["507", "128"], ",", 
   FractionBox["635", "128"], ",", 
   FractionBox["673", "128"], ",", 
   FractionBox["801", "128"], ",", 
   FractionBox["3025", "512"], ",", 
   FractionBox["3537", "512"], ",", 
   FractionBox["919", "128"], ",", 
   FractionBox["1047", "128"], ",", 
   FractionBox["218395", "32768"], ",", 
   FractionBox["251163", "32768"], ",", 
   FractionBox["260103", "32768"], ",", 
   FractionBox["292871", "32768"], ",", 
   FractionBox["556887", "65536"]}], "}"}]], "Output",
 CellChangeTimes->{3.798678283837653*^9, 3.798689273162681*^9},
 CellLabel->"Out[7]=",ExpressionUUID->"75c00f12-b549-407b-b54a-07c2f8845b62"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"%", "//", "N"}]], "Input",
 CellChangeTimes->{{3.7649322518454514`*^9, 3.764932252633027*^9}},
 CellLabel->"In[7]:=",ExpressionUUID->"496b1af1-c146-4a31-bdaf-15f736b24a72"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "1.`", ",", "1.5`", ",", "1.5`", ",", "2.375`", ",", "2.375`", ",", 
   "2.625`", ",", "2.625`", ",", "3.7109375`", ",", "3.7109375`", ",", 
   "3.9140625`", ",", "3.9140625`"}], "}"}]], "Output",
 CellChangeTimes->{3.7649322528939233`*^9},
 CellLabel->"Out[7]=",ExpressionUUID->"2af13b75-3460-4e7f-9459-2fa5dc105afb"]
}, Open  ]],

Cell[BoxData[
 RowBox[{"Clear", "[", "G", "]"}]], "Input",
 CellChangeTimes->{{3.7619972928944454`*^9, 3.7619972952962656`*^9}},
 CellLabel->"In[19]:=",ExpressionUUID->"2c2949d8-895b-4170-89bb-0e32eec4c103"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"SetDirectory", "[", "\"\<D:\>\"", "]"}]], "Input",
 CellChangeTimes->{{3.761997271823084*^9, 3.7619972784455585`*^9}},
 CellLabel->"In[4]:=",ExpressionUUID->"77b0af23-e4bc-4128-828f-ebb2d3b63856"],

Cell[BoxData["\<\"D:\\\\\"\>"], "Output",
 CellChangeTimes->{3.7619972799649267`*^9, 3.7620516176430864`*^9},
 CellLabel->"Out[4]=",ExpressionUUID->"9647fae3-bca8-4290-86c0-c4fdd08e0ff5"]
}, Open  ]],

Cell[BoxData[
 RowBox[{"Save", "[", 
  RowBox[{"\"\<state5.mx\>\"", ",", "\"\<Global`\>\""}], "]"}]], "Input",
 CellChangeTimes->{{3.757086947562703*^9, 3.7570869724954886`*^9}, 
   3.757118272676078*^9, {3.757128459407077*^9, 3.7571284601731577`*^9}, {
   3.7571340101179013`*^9, 3.757134010444265*^9}, {3.7619973161918983`*^9, 
   3.761997316362899*^9}},
 CellLabel->"In[5]:=",ExpressionUUID->"6b2645c1-7e45-48e1-ac16-9f00f15fcb63"],

Cell[BoxData[
 RowBox[{"G1", "+"}]], "Input",
 CellChangeTimes->{{3.7619962823551188`*^9, 
  3.761996322502104*^9}},ExpressionUUID->"c473064c-3135-4ffb-9b88-\
4ef7bed3db11"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"2", "/", "1.01"}]], "Input",
 CellChangeTimes->{{3.7651053107366915`*^9, 3.7651053140726557`*^9}},
 CellLabel->"In[47]:=",ExpressionUUID->"4cff22f5-9905-4049-96cf-bf35972bac39"],

Cell[BoxData["1.9801980198019802`"], "Output",
 CellChangeTimes->{3.76510531473125*^9},
 CellLabel->"Out[47]=",ExpressionUUID->"d9f1e853-c1c6-4138-b10c-dc2809c4f6c7"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"loggArray", "[", 
  RowBox[{"[", "65535", "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.814092399472937*^9, 3.8140924226186333`*^9}},
 CellLabel->
  "In[231]:=",ExpressionUUID->"a069daa4-f9e5-4785-a0a4-6fe91c674b76"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"65536", ",", "0.7077373255640336`"}], "}"}]], "Output",
 CellChangeTimes->{{3.814092406575469*^9, 3.8140924232250633`*^9}},
 CellLabel->
  "Out[231]=",ExpressionUUID->"c35faaf8-18ce-4b77-b78c-86e210b463ee"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Export", "[", 
  RowBox[{"\"\<dsads.csv\>\"", ",", "loggArray"}], "]"}]], "Input",
 CellChangeTimes->{{3.8140938247845163`*^9, 3.8140938365940237`*^9}},
 CellLabel->
  "In[234]:=",ExpressionUUID->"9c1ecc01-262b-4df1-aab8-bc51340009b7"],

Cell[BoxData["\<\"dsads.csv\"\>"], "Output",
 CellChangeTimes->{3.814093842378483*^9},
 CellLabel->
  "Out[234]=",ExpressionUUID->"73472ae7-88ca-4527-8cf1-0df3b466f641"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Directory", "[", "]"}]], "Input",
 CellChangeTimes->{{3.814093863567198*^9, 3.8140938650996532`*^9}},
 CellLabel->
  "In[235]:=",ExpressionUUID->"ef97e28d-d6b0-46d9-94c4-c0fff342c5f5"],

Cell[BoxData["\<\"/media/zhaoging/DATA\"\>"], "Output",
 CellChangeTimes->{3.814093865763824*^9},
 CellLabel->
  "Out[235]=",ExpressionUUID->"6b5b5565-a706-4b9a-9cb3-e11c546f39ed"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"loggNnAlphaArray", "[", 
   RowBox[{"[", "65535", "]"}], "]"}], "//", "N"}]], "Input",
 CellChangeTimes->{{3.814092438213253*^9, 3.814092462887823*^9}},
 CellLabel->
  "In[233]:=",ExpressionUUID->"923f90b2-eb23-4afc-bc14-3f29856986e5"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"65536.`", ",", "1.1614496320475587`"}], "}"}]], "Output",
 CellChangeTimes->{{3.814092445506812*^9, 3.814092466037249*^9}},
 CellLabel->
  "Out[233]=",ExpressionUUID->"dd7ce3b1-513e-45fe-98b1-c2ddd069592f"]
}, Open  ]]
},
WindowSize->{1440., 755.25},
WindowMargins->{{Automatic, 112.5}, {81.75, Automatic}},
Magnification:>0.8 Inherited,
FrontEndVersion->"12.1 for Linux x86 (64-bit) (March 18, 2020)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"03e5a09e-4496-403d-b567-260d62fa179c"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 211, 4, 23, "Input",ExpressionUUID->"622637be-9c56-4453-9453-78e2f9f782d5"],
Cell[772, 26, 269, 5, 23, "Input",ExpressionUUID->"4b678650-a286-4fdf-80d5-ce76be160192"],
Cell[1044, 33, 288, 6, 23, "Input",ExpressionUUID->"4ba2c2cc-6779-4d14-a0d8-237476e35efd"],
Cell[1335, 41, 284, 6, 23, "Input",ExpressionUUID->"71da341b-2543-4c5f-bd38-1907c4a5a79f"],
Cell[1622, 49, 1841, 45, 71, "Input",ExpressionUUID->"9cf03266-3f89-40ea-8d15-569937221b3d"],
Cell[3466, 96, 2072, 38, 87, "Input",ExpressionUUID->"b0136dc1-2f57-485e-9e7b-44db92e222a2"],
Cell[5541, 136, 176, 2, 23, "Input",ExpressionUUID->"e46e6da7-213a-4a2e-a523-af44086e25db"],
Cell[CellGroupData[{
Cell[5742, 142, 257, 4, 23, "Input",ExpressionUUID->"e64e6eb3-4371-4db2-b78b-9ca3176602f4"],
Cell[6002, 148, 254, 3, 26, "Output",ExpressionUUID->"32a5300b-202b-451b-833e-5166ffc1c7b0"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6293, 156, 201, 3, 23, "Input",ExpressionUUID->"e6231674-eef7-4b27-95f8-d7bec877664f"],
Cell[6497, 161, 256, 4, 26, "Output",ExpressionUUID->"524c8248-3dac-465c-a466-8d3beb4158c3"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6790, 170, 289, 4, 23, "Input",ExpressionUUID->"8c6272e2-e777-41cd-974f-2f66b810bdf9"],
Cell[7082, 176, 299, 4, 26, "Output",ExpressionUUID->"6c11bb24-2cc9-43bf-bd45-f81d85c90633"]
}, Open  ]],
Cell[7396, 183, 345, 6, 23, "Input",ExpressionUUID->"f3eb6af0-7685-48a7-8ae1-a0d1718ffff4"],
Cell[7744, 191, 420, 7, 23, "Input",ExpressionUUID->"77190dd8-fec8-43e1-8a4d-7daef74f364e"],
Cell[CellGroupData[{
Cell[8189, 202, 187, 3, 23, "Input",ExpressionUUID->"7a845324-939a-4fb5-bd24-cc9479fc776b"],
Cell[8379, 207, 846, 13, 75, "Output",ExpressionUUID->"acb0a4a1-2a4d-4121-bb79-29b863b7b9d3"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9262, 225, 176, 2, 23, "Input",ExpressionUUID->"b44b6267-35ef-4584-803f-c05853a1d8b2"],
Cell[9441, 229, 148, 2, 26, "Output",ExpressionUUID->"2441f3d2-2d9b-4e9e-89fb-bf9a2e86f134"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9626, 236, 493, 12, 23, "Input",ExpressionUUID->"4624f07f-ec29-471a-b302-82cae5b9c0c7"],
Cell[10122, 250, 246, 4, 40, "Output",ExpressionUUID->"e63cf172-7c4f-4d75-a890-301418a2243f"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10405, 259, 173, 2, 23, "Input",ExpressionUUID->"8387b25b-ff0c-410b-873c-05faafb66bd6"],
Cell[10581, 263, 197, 3, 40, "Output",ExpressionUUID->"59d7dca0-8b27-4170-b794-d44642fcc2c7"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10815, 271, 597, 13, 23, "Input",ExpressionUUID->"0a947f28-ffa5-46e5-b60e-d09ac427f944"],
Cell[11415, 286, 905, 23, 40, "Output",ExpressionUUID->"9822dd6a-51b3-4cdb-b5af-391eb195cd42"]
}, Open  ]],
Cell[CellGroupData[{
Cell[12357, 314, 367, 10, 23, "Input",ExpressionUUID->"3461529b-98be-448f-a9e6-788a6185f5ee"],
Cell[12727, 326, 907, 23, 40, "Output",ExpressionUUID->"75c00f12-b549-407b-b54a-07c2f8845b62"]
}, Open  ]],
Cell[CellGroupData[{
Cell[13671, 354, 196, 3, 23, "Input",ExpressionUUID->"496b1af1-c146-4a31-bdaf-15f736b24a72"],
Cell[13870, 359, 360, 7, 26, "Output",ExpressionUUID->"2af13b75-3460-4e7f-9459-2fa5dc105afb"]
}, Open  ]],
Cell[14245, 369, 207, 3, 23, "Input",ExpressionUUID->"2c2949d8-895b-4170-89bb-0e32eec4c103"],
Cell[CellGroupData[{
Cell[14477, 376, 220, 3, 23, "Input",ExpressionUUID->"77b0af23-e4bc-4128-828f-ebb2d3b63856"],
Cell[14700, 381, 187, 2, 26, "Output",ExpressionUUID->"9647fae3-bca8-4290-86c0-c4fdd08e0ff5"]
}, Open  ]],
Cell[14902, 386, 434, 7, 23, "Input",ExpressionUUID->"6b2645c1-7e45-48e1-ac16-9f00f15fcb63"],
Cell[15339, 395, 173, 4, 23, "Input",ExpressionUUID->"c473064c-3135-4ffb-9b88-4ef7bed3db11"],
Cell[CellGroupData[{
Cell[15537, 403, 201, 3, 23, "Input",ExpressionUUID->"4cff22f5-9905-4049-96cf-bf35972bac39"],
Cell[15741, 408, 166, 2, 26, "Output",ExpressionUUID->"d9f1e853-c1c6-4138-b10c-dc2809c4f6c7"]
}, Open  ]],
Cell[CellGroupData[{
Cell[15944, 415, 240, 5, 23, "Input",ExpressionUUID->"a069daa4-f9e5-4785-a0a4-6fe91c674b76"],
Cell[16187, 422, 246, 5, 26, "Output",ExpressionUUID->"c35faaf8-18ce-4b77-b78c-86e210b463ee"]
}, Open  ]],
Cell[CellGroupData[{
Cell[16470, 432, 259, 5, 23, "Input",ExpressionUUID->"9c1ecc01-262b-4df1-aab8-bc51340009b7"],
Cell[16732, 439, 169, 3, 26, "Output",ExpressionUUID->"73472ae7-88ca-4527-8cf1-0df3b466f641"]
}, Open  ]],
Cell[CellGroupData[{
Cell[16938, 447, 208, 4, 23, "Input",ExpressionUUID->"ef97e28d-d6b0-46d9-94c4-c0fff342c5f5"],
Cell[17149, 453, 180, 3, 26, "Output",ExpressionUUID->"6b5b5565-a706-4b9a-9cb3-e11c546f39ed"]
}, Open  ]],
Cell[CellGroupData[{
Cell[17366, 461, 270, 6, 23, "Input",ExpressionUUID->"923f90b2-eb23-4afc-bc14-3f29856986e5"],
Cell[17639, 469, 246, 5, 26, "Output",ExpressionUUID->"dd7ce3b1-513e-45fe-98b1-c2ddd069592f"]
}, Open  ]]
}
]
*)

